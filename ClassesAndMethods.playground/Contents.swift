// in Swift you do not necessarily need to subclass anything (unlike Obj-C, where you must subclass
// NSObject or something that derives from it
class TipCalculator {
    // creating the properties on a class - must set initial value on declaration, or in your initializer
    // alternately, declare them as optional so you don't have to set the properties to an initial value
    let total: Double
    let taxPct: Double
    let subtotal: Double
    
    // create an initializer for the class taking two params 
    // always named init in Swift, if you have multiple initializers, they must take different params
    init(total: Double, taxPct: Double) {
        self.total = total // distinguishes between the parameters of this method and the props of the class
        self.taxPct = taxPct
        subtotal = total/(taxPct + 1)
    }
    
    // method, -> return type
    func calcTipWithTipPct(tipPct: Double) -> Double {
        return subtotal * tipPct
    }
    
    // method returns a dictionary (key: Int, value: Double), short for Dictionary<Int, Double>
    func returnPossibleTips() -> [Int: Double] {
        let possibleTipsInferred = [0.15, 0.18, 0.20]
        var retVal = [Int: Double]() // declare dict as a var b/c we are modifying it
        for possibleTip in possibleTipsInferred {
            let intPct = Int(possibleTip*100)
            retVal[intPct] = calcTipWithTipPct(possibleTip)
        }
        return retVal
    }
    
    func printPossibleTips() {
        let possibleTipsInferred = [0.15, 0.18, 0.20]
        let possibleTipsExplicit:[Double] = [0.15, 0.18, 0.20] // [Double] shortcut for Array<Double>
        for possibleTip in possibleTipsInferred {
            print("\(possibleTip*100)%: \(calcTipWithTipPct(possibleTip))")
        }
        /*print("15%: \(calcTipWithTipPct(0.15))")
        print("18%: \(calcTipWithTipPct(0.18))")
        print("20%: \(calcTipWithTipPct(0.20))")*/
    }
}

// create an instance of the tip calculator, call the method to print the possible tips
let tipCalc = TipCalculator(total: 33.25, taxPct: 0.06)
tipCalc.returnPossibleTips()