//: Playground - noun: a place where people can play

import UIKit
import Foundation

// a list of methods specifying a "contract"/"interface"
// the @objc tag signifies a protocol with optional methods
@objc protocol Speaker {
    func Speak() // any class conforming to the protocol implements this method
    optional func TellJoke() // prefix optional methods with this optional tag
}

class Floppy: Speaker {
    @objc func Speak() { // must annotate all methods implementing the protocol with @objc
        print("Hello, I am Floppy!")
    }
    @objc func TellJoke() {
        print("Why did the chicken cross the road?")
    }
}

class Poppy: Speaker {
    @objc func Speak() {
        print("I am Poppy!")
    }
    @objc func TellJoke() {
        print("Why did the turtle cross the road?")
    }
    @objc func Plot() {
        print("I'm actually thinking of world domination.")
    }
}

protocol DateSimulatorDelegate {
    func dateSimulatorDidStart(sim: DateSimulator, a: Speaker, b: Speaker)
    func dateSimulatorDidEnd(sim: DateSimulator, a: Speaker, b: Speaker)
}

// delegate: variable conforming to a protocol, which a class typically uses to notify of events or perform various sub-tasks 
class DateSimulator {
    let a: Speaker
    let b: Speaker
    init (a: Speaker, b: Speaker) {
        self.a = a
        self.b = b
    }
    // optional because DateSimulator should work fine regardless of whether the delegate is set
    var delegate: DateSimulatorDelegate?
    func simulate() {
        // optional chaining: check if nil
        delegate?.dateSimulatorDidStart(self, a: self.a, b: self.b)
        print("Off to dinner...\n")
        a.Speak()
        b.Speak()
        print("Walking back home...\n")
        a.TellJoke?()
        b.TellJoke?()
        delegate?.dateSimulatorDidEnd(self, a: self.a, b: self.b)
    }
}

class LoggingDateSimulator: DateSimulatorDelegate {
    func dateSimulatorDidStart(sim: DateSimulator, a: Speaker, b: Speaker) {
        print("Date started!")
    }
    
    func dateSimulatorDidEnd(sim: DateSimulator, a: Speaker, b: Speaker) {
        print("Date ended!")
    }
}

let sim = DateSimulator(a:Floppy(), b:Poppy())
sim.delegate = LoggingDateSimulator()
sim.simulate()