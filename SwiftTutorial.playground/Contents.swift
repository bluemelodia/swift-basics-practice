// constants: their value cannot change, preferred to use
// this as it allows the compiler to perform optimizations

    // explicit type setting
    let swiftTeam: Int = 13

    // implicit type setting, compiler had enough info
    // to do it automatically, this is preferred (concise
    // and easier to read)
    let iOSTeam = 54
    let otherTeams = 48

// a variable: value can change
var totalTeam = swiftTeam + iOSTeam + otherTeams

totalTeam += 1

// Double is the default for inferring decimal values
let priceInferred = 19.99
let priceExplicit: Double = 19.99

// Bools
let onSaleInferred = true
let onSaleExplicit: Bool = false

// Strings
let nameInferred = "Whoopie Cushion"
let nameExplicit: String = "Whoopie Cushion"

// if statements, string interpolation
// braces are required even for one-line staetments
if onSaleInferred {
    // string interpolation: to substitue a string, use 
    // \(your expression)
    print("\(nameInferred) on sale for \(priceInferred)!")
} else {
    print("\(nameInferred) at regular price: \(priceInferred)!")
}




