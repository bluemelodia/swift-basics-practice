//: Playground - noun: a place where people can play

import UIKit

// unnamed tuple
let tipAndTotal = (4.00, 25.19) // or let tipAndTotal:(Double, Double) = (4.00, 25.19)
// accessing by index
tipAndTotal.0
tipAndTotal.1

// decomposing by name, making a new constant referring to each element in the tuple
let (tipAmt, theTotal) = tipAndTotal
tipAmt
theTotal

// named tuples - name the tuples at declaration time 
let tipAndTotalNamed = (tipAmt:4.00, total:25.19)
tipAndTotalNamed.tipAmt
tipAndTotalNamed.total

// explicit : let tipAndTotalNamed:(tipAmt:Double, total:Double) = (4.00, 25.19) 

// returning tuples
let total = 21.19
let taxPct = 0.06
let subtotal = total/(taxPct + 1)
func calcTipWithTipPct(tipPct:Double) -> (tipAmt:Double, total:Double) {
    let tipAmt = subtotal*tipPct
    let finalTotal = total+tipAmt
    return(tipAmt, finalTotal)
}
calcTipWithTipPct(0.20)
