//
//  TipCalculatorModel.swift
//  TipCalculator
//
//  Created by Melanie Lislie Hsu on 12/27/15.
//  Copyright © 2015 Melanie Lislie Hsu. All rights reserved.
//

// Model class representing class's data & ops that our app will perform on that data

import Foundation
import UIKit // needed to use UITableVIew

class TipCalculatorModel {
    // creating the properties on a class - must set initial value on declaration, or in your initializer
    // alternately, declare them as optional so you don't have to set the properties to an initial value
    var total: Double
    var taxPct: Double
    // a computed property that doesn't actually store a value, but instead is computed each time
    // based on other values - this one is recomputed each time it's accessed based on the current values
    // of total and taxPct
    var subtotal: Double {
        get {
            return total/(taxPct + 1)
        }
    }
    
    // create an initializer for the class taking two params
    // always named init in Swift, if you have multiple initializers, they must take different params
    init(total: Double, taxPct: Double) {
        self.total = total // distinguishes between the parameters of this method and the props of the class
        self.taxPct = taxPct
    }
    
    // method, -> return type
    func calcTipWithTipPct(tipPct: Double) -> (tipAmt:Double, total:Double) {
        let tipAmt = subtotal * tipPct
        let finalTotal = total + tipAmt
        return (tipAmt, finalTotal)
    }
    
    // method returns a dictionary (key: Int, value: Double), short for Dictionary<Int, Double>
    func returnPossibleTips() -> [Int: (tipAmt:Double, total:Double)] {
        let possibleTipsInferred = [0.15, 0.18, 0.20]
        var retVal = [Int: (tipAmt:Double, total:Double)]() // declare dict as a var b/c we are modifying it
        for possibleTip in possibleTipsInferred {
            //let (tipAmt, total) = calcTipWithTipPct(possibleTip)
            let intPct = Int(possibleTip*100)
            retVal[intPct] = calcTipWithTipPct(possibleTip)
        }
        return retVal
    }
    
    func printPossibleTips() {
        let possibleTipsInferred = [0.15, 0.18, 0.20]
        let possibleTipsExplicit:[Double] = [0.15, 0.18, 0.20] // [Double] shortcut for Array<Double>
        for possibleTip in possibleTipsInferred {
            print("\(possibleTip*100)%: \(calcTipWithTipPct(possibleTip))")
        }
        /*print("15%: \(calcTipWithTipPct(0.15))")
        print("18%: \(calcTipWithTipPct(0.18))")
        print("20%: \(calcTipWithTipPct(0.20))")*/
    }
}

// class must extend NSObject in order to implement UITableViewDataSource
class TestDataSource: NSObject, UITableViewDataSource {
    let tipCalc = TipCalculatorModel(total: 33.25, taxPct: 0.06)
    // keep these as vars so that we can change them over time
    var possibleTips = Dictionary<Int, (tipAmt:Double, total:Double)>()
    var sortedKeys:[Int] = []
    // we're overridng the init method from NSObject
    override init() {
        possibleTips = tipCalc.returnPossibleTips()
        sortedKeys = Array(possibleTips.keys).sort()
        super.init()
    }
    
    // required method to confirm to UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedKeys.count
    }
    
    // called for each row in table view, returns the view representing this row, which is a subclass of UITableViewCell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // create table view cells with a built-in style, or cereate your own custom subclasses for your own style - here, we're creating a table view cell with a default style
        let cell = UITableViewCell(style: UITableViewCellStyle.Value2, reuseIdentifier: nil)
        // indexPath is a simple collection of the section and row for this table view cell; since there's just one section, you use the row to pull out the appropriate tip percentage to display from sortedKeys
        let tipPct = sortedKeys[indexPath.row]
        // make a variable for each element in the Tuple for that tip percentage, since we're sure there's something for the dictionary key in this case, we use ! for forced unwrapping; when we access an item in a dictionary, we get an optional value, since it's possible there is nothing in the dictionary for that particular key
        let tipAmt = possibleTips[tipPct]!.tipAmt
        let total = possibleTips[tipPct]!.total
        // the built-in UITableView cell has two props for its sublabels: textLabel and detailTextLabel
        cell.textLabel?.text = "\(tipPct)%:"
        cell.detailTextLabel?.text = String(format: "Tip: $%0.2f, Total: $%0.2f", tipAmt, total)
        return cell
    }
}