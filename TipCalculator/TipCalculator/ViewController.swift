//
//  ViewController.swift
//  TipCalculator
//
//  Created by Melanie Lislie Hsu on 12/27/15.
//  Copyright © 2015 Melanie Lislie Hsu. All rights reserved.
//

// View controller manages the communication between your views and your model.

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    // ! means the vars are optional values, but they're implicitly unwrapped (you can write code assuming that they
    // are set, and your app will crash if they aren't set - convenient way to create vars you know for sure will be 
    // set up before you use them (like UI elements in the Storyboard), so you don't have to unwrap the optionals 
    // each time you use them
    @IBOutlet var totalTextField: UITextField!
    @IBOutlet var taxPctSlider: UISlider!
    @IBOutlet var taxPctLabel: UILabel!
    @IBOutlet var resultsTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    // callbacks for actions from views must always have this signature - funct with no return val, takes one param
    // of type AnyObject (representing a class of any type) - AnyObjet equv of id in Objective-C
    @IBAction func calculateTapped(sender: AnyObject) {
        // convert a String to a Double
        tipCalc.total = Double((totalTextField.text! as NSString).doubleValue)
        // call returnPossibleTips method in tipCalc model, which returns a dict of possible tip percentages mapped to tip values
        possibleTips = tipCalc.returnPossibleTips()
        sortedKeys = Array(possibleTips.keys).sort()
        tableView.reloadData()
    }
    @IBAction func taxPercentageChanged(sender: AnyObject) {
        tipCalc.taxPct = Double(taxPctSlider.value)/100.0
        refreshUI()
    }
    // dismisses the keyboard
    @IBAction func viewTapped(sender: AnyObject) {
        totalTextField.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        refreshUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    let tipCalc = TipCalculatorModel(total: 33.25, taxPct: 0.06)
    var possibleTips = Dictionary<Int, (tipAmt:Double, total:Double)>()
    var sortedKeys:[Int] = []
    
    func refreshUI() {
        // must be explicit when converting one type to another
        totalTextField.text = String(format: "%0.2f", tipCalc.total)
        // must cast because taxPctSlider.value is a float
        taxPctSlider.value = Float(tipCalc.taxPct)*100.0
        // update the value based on the tax percentage
        taxPctLabel.text = "Tax Percentage (\(Int(taxPctSlider.value))%)"
    }

    // required method to confirm to UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedKeys.count
    }
    
    // called for each row in table view, returns the view representing this row, which is a subclass of UITableViewCell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // create table view cells with a built-in style, or cereate your own custom subclasses for your own style - here, we're creating a table view cell with a default style
        let cell = UITableViewCell(style: UITableViewCellStyle.Value2, reuseIdentifier: nil)
        // indexPath is a simple collection of the section and row for this table view cell; since there's just one section, you use the row to pull out the appropriate tip percentage to display from sortedKeys
        let tipPct = sortedKeys[indexPath.row]
        // make a variable for each element in the Tuple for that tip percentage, since we're sure there's something for the dictionary key in this case, we use ! for forced unwrapping; when we access an item in a dictionary, we get an optional value, since it's possible there is nothing in the dictionary for that particular key
        let tipAmt = possibleTips[tipPct]!.tipAmt
        let total = possibleTips[tipPct]!.total
        // the built-in UITableView cell has two props for its sublabels: textLabel and detailTextLabel
        cell.textLabel?.text = "\(tipPct)%:"
        cell.detailTextLabel?.text = String(format: "Tip: $%0.2f, Total: $%0.2f", tipAmt, total)
        return cell
    }
}

