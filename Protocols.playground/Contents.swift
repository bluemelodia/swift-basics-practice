//: Playground - noun: a place where people can play

import UIKit
import Foundation

// a list of methods specifying a "contract"/"interface"
// the @objc tag signifies a protocol with optional methods
@objc protocol Speaker {
    func Speak() // any class conforming to the protocol implements this method
    optional func TellJoke() // prefix optional methods with this optional tag
}

// colon after class name marks a class a conforming to a protocol, list the protocol after the colon
// and after the name of the class if inherits from, if any
class Vicki: Speaker {
    @objc func Speak() { // must annotate all methods implementing the protocol with @objc
        print("Hello, I am Vicki!")
    }
}

class Ray: Speaker {
    @objc func Speak() {
        print("Yo, I am Ray!")
    }
    @objc func TellJoke() {
        print("I have a PhD in Horribleness.")
    }
    @objc func Plot() {
        print("I'm actually a turtle.")
    }
}

// like with ObjC, you can only inherit from 1 class in Swift
class Animal {
}
class Turtle: Animal, Speaker {
    @objc func Speak() {
        print("Turtle!")
    }
}

// Because we declared the speaker as speaker, you can only call methods on speaker that exist in the Speaker
// protocol, however casting speaker back to Ray lets it call other methods
var speaker: Speaker
speaker = Ray()
speaker.Speak()
(speaker as! Ray).Plot()
speaker = Vicki()
(speaker as! Vicki).Speak()
// optional chaining: if you put a ? mark after the method name, it will check if the method exists before calling it; otherwise, it will behave as if you called a method that returns nil
speaker.TellJoke?()